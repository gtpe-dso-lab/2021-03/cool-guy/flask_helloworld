*** Variables ***
${URL}      ${Staging.URL}

*** Settings ***
Library    String
Library    REST           ${URL}          ssl_verify=false

*** Test Cases ***
Test json endpoint
  GET       /json
  Output    response body
  Object    response body
  String    response body data	hello_georgia
