*** Settings ***
Library    Selenium2Library
Suite Teardown  Close All Browsers

*** Variables ***
${URL}      ${Staging.URL}
${BROWSER}  Chrome

*** Test Cases ***
Test root endpoint
  ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
  Call Method    ${chrome_options}   add_argument    headless
  Call Method    ${chrome_options}   add_argument    disable-gpu
  Call Method    ${chrome_options}   add_argument    no-sandbox

  ${options}=     Call Method     ${chrome_options}    to_capabilities
  Open Browser    ${URL}          browser=${BROWSER}   desired_capabilities=${options}
  Selenium2Library.Wait Until Page Contains  Current Time in Atlanta


